# Fuxia's Userstyles

Userstyles are files that can change the layout of a given website.
In order to use them you need the **[Stylus Extension](https://add0n.com/stylus.html)** 
for your browser. It's available for Opera, Firefox, and all Chromium based 
browsers like Google Chrome or Vivaldi.

Click on any style file you like and confirm the installation. That's it already.


## Hide move times on chess24

**[Install](https://gitlab.com/fuxia/userstyles/-/raw/master/chess24-Hide.move.times.user.css)**

As the name says, it hides the move times in the game notation. The result looks like this:

![Screenshot Hide move times](images/hide-move-times.png)

## chess24 Grey Board

**[Install](https://gitlab.com/fuxia/userstyles/-/raw/master/chess24-grey-board.user.css)**

Clean, grey board for chess24. Optimal contrast between the squares and the pieces.
The images are in the stylesheet, so no external request is necessary.

![Screenshot chess24 grey board](images/chess24-grey-board.user.png)

## chess24 Editor Enhancements

**[Install](https://gitlab.com/fuxia/userstyles/-/raw/master/chess24-editor-enhancements.user.css)**

This is only useful for people who can edit articles on chess24. 
It makes the image caption field larger, and the HTML view more readable.
The "Save" button is always visible on the right side, and it's a bit larger.

## Lichess Grey Board

**[Install](https://gitlab.com/fuxia/userstyles/-/raw/master/lichess-grey-board.user.css)**

A nice clean grey board for lichess. It's as neutral as possible, but with a 
better contrast than lichess’ own boards. Very similar to the chess24 board.

![Screenshot Lichess grey board](images/lichess-grey-board.png)

## Lichess Clean Inbox

**[Install](https://gitlab.com/fuxia/userstyles/-/raw/master/lichess-clean-inbox.user.css)**

Some time ago Lichess decided to present the inbox messages in a bubble style 
like one of these "trendy" messengers … I hate that. This stylesheet turns the 
inbox into a plain, readable view, similar to a classic email reader. Works 
with the light and the dark theme.

![Screenshot Lichess clean inbox](images/lichess-clean-inbox.png)



## Lichess Hide takeback button

**[Install](https://gitlab.com/fuxia/userstyles/-/raw/master/lichess-hide-takeback.user.css)**

Removes the useless takeback button when you have disabled takebacks.

## Lichess Hide spectators

**[Install](https://gitlab.com/fuxia/userstyles/-/raw/master/lichess-hide-spectators.user.css)**

Hides the spectators when you are playing or viewing a game.